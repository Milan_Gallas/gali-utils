<?php
	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 4.12.2016
	 * Time: 14:12
	 */

	namespace Gali\Utils;


	class Arrays
	{
		/**
		 * Vrátí všechny klíče z pole
		 *
		 * @return type
		 */
		public function keys(Array $array)
		{
			return array_keys($array);
		}

		/**
		 * Vrátí klíč podle zvolené hodnoty
		 *
		 * @param type $value
		 * @return type key
		 */
		public function selectKey(Array $array, $value)
		{
			return array_search($value, $array);
		}

		/**
		 * Vrátí počet záznamů v poli
		 *
		 * @return type
		 */
		public function count(Array $array)
		{
			return count($array);
		}

		/**
		 * Zjistí zda se v poli nachází zvolená položka.
		 * "Mžeme hledat v hodnotách, nebo v klíčích.
		 *
		 * @param $array kolekce, ve které chceme hledat
		 * @param $param $value
		 * @param $findInKeys - pokud je true hledá v poli klíčů dané kolekce
		 * @return Boolean
		 */
		public function contains(Array $array, $param, $findInKeys = false)
		{
			foreach ($array as $item => $value) {
				if ($findInKeys) {
					if ($item == $param) {
						return true;
					}
				} else {
					if ($value == $param) {
						return true;
					}
				}
			}

			return false;
		}

		/**
		 * Seřadí pole podle hodnot, nebo podle klíčů
		 *
		 * @param Boolean $key
		 * @return array
		 */
		public function sort(Array $array, $key = false)
		{
			return ($key) ? ksort($array) : sort($array);
		}

		/**
		 * Načte xml soubor a vrátí ho jako pole
		 *
		 * @param type $xml
		 * @return type
		 */
		public function xmlDecode($xml)
		{
			$simpleXMLElement = simplexml_load_string($xml);
			$json = json_encode($simpleXMLElement);

			return json_decode($json, true);
		}

		/**
		 * Příjme pole a uloží ho jako xml soubor
		 *
		 * @param array $array
		 * @param type $root
		 * @param string $type
		 * @return type
		 */
		public function xmlEncode(array $input, $root, $fileName = '')
		{
			$doc = new \DOMDocument('1.0', 'UTF-8');
			$doc->formatOutput = true;

			$rootElement = $doc->createElement($root);
			$doc->appendChild($rootElement);
			$this->xmlEncodeElement($input, $rootElement);

			if($fileName != ''){
				return $doc->save($fileName);
			}

			return $doc->saveXML();
		}

		/**
		 * Pomocná metoda třídy, která se stará o skládání výsledého xml
		 *
		 * @param array $array
		 * @param \DOMElement $parent
		 */
		private function xmlEncodeElement(array $input, \DOMElement $parent)
		{
			foreach ($input as $key => $value)
			{
				$element = $parent->ownerDocument->createElement($key);
				$parent->appendChild($element);
				if (is_array($value))
					$this->xmlEncodeElement($value, $element);
				else
				{
					$text = $parent->ownerDocument->createTextNode($value);
					$element->appendChild($text);
				}
			}
		}
	}