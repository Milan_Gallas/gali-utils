<?php
	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 4.12.2016
	 * Time: 14:02
	 */

	namespace Gali\Utils\Enum;


	class SettingsParam
	{
		const THOUSANDS_SEP = 'thousands_sep';
		const FLOAT = 'decimal_sep';
	}