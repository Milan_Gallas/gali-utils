<?php
	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 4.12.2016
	 * Time: 13:49
	 */

	namespace Gali\Utils;


	class Floats
	{
		/**
		 * Převede na číslo s desetiným místem
		 * @param $num
		 * @return float
		 */
		public function tofloat($num)
		{
			$num = (string)$num;
			$dotPos = strrpos($num, '.');
			$commaPos = strrpos($num, ',');
			$sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
				((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

			if (!$sep) {
				return floatval(preg_replace("/[^0-9]/", "", $num));
			}

			return floatval(
				preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
				preg_replace("/[^0-9]/", "", substr($num, $sep + 1, strlen($num)))
			);
		}
	}