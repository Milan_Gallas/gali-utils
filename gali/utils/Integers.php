<?php
	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 4.12.2016
	 * Time: 13:56
	 */

	namespace Gali\Utils;
	use Gali\Utils\Enum\SettingsParam;

	class Integers
	{
		/**
		 * Defaultní počet desetinýh míst
		 */
		const DEFAULT_DECIMAL_NUMBER = 2;

		/**
		 * Defaultní nastavení
		 * Používá se pokud, není napojení na user_settings v databázi v LIGHT verzi
		 * @var array
		 */
		protected $defaultSettings = array(
			'thousands_sep' => ' ',
			'decimal_sep' => ','
		);

		/**
		 * Naformatuje cislo podla oddelovacu tisicu a desatinnych mist
		 * @param integer $number
		 * @param string $thousandsSeparator
		 * @param string $decimalSeparator
		 * @return string
		 */
		public function formatInteger($number, $renderDecimal = false, $thousandsSeparator = null, $decimalSeparator = null)
		{
			$thousandsSeparator = !is_null($thousandsSeparator) ?: $this->geSettings(SettingsParam::THOUSANDS_SEP);
			$decimalSeparator = !is_null($decimalSeparator) ?: $this->geSettings(SettingsParam::FLOAT);
			$decimalNumber = $renderDecimal ? self::DEFAULT_DECIMAL_NUMBER : 0;
			return number_format($number, $decimalNumber, $decimalSeparator, $thousandsSeparator);
		}

		/**
		 * @param key = klíč nastavení
		 * @return array
		 */
		public function geSettings($key)
		{
			return $this->defaultSettings[$key];
		}

		/**
		 * @param array $defaultSettings
		 */
		public function setSettings($defaultSettings)
		{
			$this->defaultSettings = $defaultSettings;
		}

	}