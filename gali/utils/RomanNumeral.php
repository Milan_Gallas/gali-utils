<?php

	namespace Gali\Utils;

	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 4.12.2016
	 * Time: 1:42
	 */
	class RomanNumeral
	{
		/**
		 * Překladové pole. Toto pole slouží jako slovník mezi normálními čísli a římskými čislicemi
		 * @var array
		 */
		private $romanToNumber = array(
			'M'  => 1000,
			'CM' => 900,
			'D'  => 500,
			'CD' => 400,
			'C'  => 100,
			'XC' => 90,
			'L'  => 50,
			'XL' => 40,
			'X'  => 10,
			'IX' => 9,
			'V'  => 5,
			'IV' => 4,
			'I'  => 1,
		);

		/** Převod na římské číslice
		 *
		 * @param int
		 * @return string
		 */
		public function toRoman($normalNumber)
		{
			$return = '';
			foreach ($this->romanToNumber as $roman => $number) {
				$times = floor($normalNumber / $number);
				$return .= str_repeat($roman, $times);
				$normalNumber -= $number * $times;
			}

			return $return;
		}

		/**
		 * Převod z římských číesl na normální
		 * @param $romanNumber
		 * @return int
		 * @throws \Exception
		 */
		public function fromRoman($romanNumber)
		{
			$return = 0;
			$pos = 0;
			foreach ($this->romanToNumber as $roman => $number) {
				while ($pos < strlen($romanNumber) && substr_compare($romanNumber, $roman, $pos, strlen($roman)) == 0) {
					$return += $number;
					$pos += strlen($roman);
				}
			}
			if ($pos != strlen($romanNumber)) {
				throw new \Exception("Invalid roman number: $romanNumber");
			}

			return $return;
		}
	}