<?php
	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 4.12.2016
	 * Time: 1:56
	 */

	namespace Gali\Utils;


	class Math
	{
		/**
		 * Vrátí průměrnou hodnotu čísel
		 *
		 * @param array $numbers
		 * @return Integer
		 */
		public function average(Array $numbers)
		{
			return array_sum($numbers) / count($numbers);
		}

		/**
		 * Vrátí medián dané číselné množiny
		 *
		 * @param array $numbers
		 * @return integer
		 */
		public function median(Array $numbers)
		{
			//získáme počet prvků!
			$count = count($numbers);

			//setřídíme pole
			sort($numbers);

			//pokud je sudý počet prvků
			if ($count % 2 == 0) {
				$number1 = $this->getNumber($numbers, $count / 2);
				$number2 = $this->getNumber($numbers, ($count / 2) + 1);

				return (($number1 + $number2) / 2);
			} //pokud je lichý počeet prvků
			else {
				$item = (($count + 1) / 2);

				return $this->getNumber($numbers, $item);
			}
		}

		/**
		 * Vrátí číslo, které se v poli vyskytuje nejvíce krát
		 *
		 * @param type $numbers
		 * @return Array
		 */
		public function modus($numbers)
		{
			$map = array();

			//zpočítám kolikrát se daný prvek v poli vyskytuje
			foreach ($numbers as $number) {
				if (@$map[$number]) {
					$map[$number]++;
				} else {
					$map[$number] = 1;
				}
			}

			//seznam čísel, které mají stejý počet výskytů, jako najvyšší hodnota
			$max = max($map);
			$max_array = array();
			foreach ($map as $key => $value) {
				if ($value === $max) {
					$max_array[] = $key;
				}
			}

			//vrátím čísla
			return $max_array;
		}

		/**
		 * Vrátí rozptyl dané číselné množiny
		 *
		 * @param array $numbers
		 * @return type
		 */
		public function dispersion(Array $numbers)
		{
			$avg = $this->average($numbers);
			$result = array_map(function ($number) use ($avg) {
				return ($number - $avg) * ($number - $avg);
			}, $numbers);

			return (1 / $avg) * array_sum($result);
		}

		/**
		 * Vrátí směrodatnou odchylku
		 *
		 * @param array $numbers
		 * @return type
		 */
		public function authoritativeDeviation(array $numbers)
		{
			$disprersion = $this->dispersion($numbers);

			return $disprersion * $disprersion;
		}

		/**
		 * Vrátí faktoriál daného čísl
		 * Tímto způsobem se počítají i permutace
		 *
		 * @param type $cislo
		 * @return int
		 * @throws \Exception
		 */
		public function factorial($number)
		{
			if ($number < 0) {
				throw new \Exception ("not zero factorial");
			}
			if ($number < 2) {
				return 1;
			} else {
				return $number * $this->factorial($number - 1);
			}
		}

		/**
		 * Počet možností máme li $k prvků z n množiny prvků. Zde záleží na pořadí prvků.
		 * Například V(3,7) = 210
		 * V(počet umistění z, n prvků)
		 * http://www.matematika.cz/variace
		 *
		 * @param iteger $k
		 * @param integer $n
		 * @param bolean $repeat - pokud je true, tak se počítají variace s opakováním
		 * @return type
		 */
		public function variations($k, $n, $repeat = false)
		{
			if (!$repeat) {
				return $this->factorial($n) / $this->factorial($n - $k);
			}

			return pow($n, $k);
		}

		/**
		 * Vrátí počet kombinací. Nezáleží na poředí prvků.
		 * Například sportka 6 čísel z 49.
		 * * @param iteger $k
		 *
		 * @param integer $n
		 * @param bolean $repeat - pokud je true, tak se počítají kombinace s opakováním
		 * @return float
		 */
		public function combinations($k, $n, $repeat = false)
		{
			if (!$repeat) {
				return $this->variations($k, $n) / $this->factorial($k);
			}

			return $this->combinations($k - 1, $n + $k - 1);
		}

		/**
		 * Vrátí prvek z pole. Počítá z posunem
		 *
		 * @param type $array
		 * @param type $key
		 * @return type
		 */
		protected function getNumber($array, $key)
		{
			return $array[$key - 1];
		}
	}