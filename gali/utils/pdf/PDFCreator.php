<?php
	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 20.6.2016
	 * Time: 10:17
	 */

	namespace Gali\Utils\Pdf;

	class PDFCreator
	{
		/** @var  \mPDF */
		protected $pdf;

		/**
		 * Vytvoří novou instanci třídy mPDF
		 */
		protected function createInstance()
		{
			$this->pdf = new \mPDF();
		}

		/**
		 * Nejdříve vytvoří instanci mPDF
		 * Vloží kaskádové styly a vygenerované html do nového pdf dokumentu.
		 * Poté se zavolá metoda outputProcess
		 * @param $html
		 * @param string $outputName
		 * @param $cssSource
		 */
		public function printPDF($html, $outputName, $cssSource = null, $output)
		{
			$this->createInstance();
			//každý výstup má svoji pozici. Pokud předáme styly zapíší se jako první
			$position = 1;
			//zapíšeme styly, pokus existují
			if($cssSource){
				$cssSource = $this->createCssSource($cssSource);
				$this->pdf->WriteHTML(file_get_contents($cssSource), $position);
				$position++;
			}
			//zapíšu vygenerované html
			$this->pdf->WriteHTML($html, $position);
			//vyvolám stažení dokumentu
			$this->outputProcess($outputName, $output);
		}

		/**
		 * Defaultní nastavení je zatím stažení dokumentu přímo v prohlížeči uživatele
		 * @param $outputName
		 */
		protected function outputProcess($outputName, $output)
		{
			$this->pdf->Output($outputName.".pdf",$output);
		}

		/**
		 * @param $cssSource
		 * @return string
		 */
		protected function createCssSource($cssSource)
		{
			//zde může být další logika
			return $cssSource;
		}
	}