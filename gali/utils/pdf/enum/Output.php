<?php

	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 4.12.2016
	 * Time: 19:23
	 */
	namespace Gali\Utils\Pdf\Enum;

	/**
	 * Class Output
	 *
	 * @package Gali\Utils\Pdf\Enum
	 */
	class Output
	{
		/**
		 * Stáhne v prohlížeči
		 */
		const DOWNLOAD = 'D';
		/**
		 * Zobrazí v prohlížečí
		 */
		const SHOW = 'I';
		/**
		 * Vrátí jako string. Můžu poté použít například v mailu
		 */
		const STRING = 'S';
		/**
		 * Stáhne lokálně
		 */
		const LOCAL_DOWNLOAD = 'F';
	}