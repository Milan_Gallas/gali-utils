<?php
	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 20.6.2016
	 * Time: 10:38
	 */

	namespace Gali\Utils\Pdf\Test;


	use Gali\Utils\Pdf\Template\BasePdfTemplate;

	class TestPdfTemplate extends BasePdfTemplate
	{

		/**
		 * @return string
		 */
		public function getSource()
		{
			return __DIR__.'/test.php';
		}
	}