<div class="invoice">
    <div class="half-box">
        <h1>Faktura - daňový doklad</h1>
    </div>

    <div class="half-box">
        <h1 class="text-right">č. <?= htmlspecialchars($order['number'], true) ?></h1>
    </div>

    <div class="clear"></div>

    <hr class="invoice-hr"/>

    <div class="half-box">
        <h2>Dodavatel</h2>
        <p>
            <strong><?= htmlspecialchars($seller['name'], true) ?></strong>
        </p>
        <p>
            <?= htmlspecialchars($seller['address'], true) ?>
        </p>
        <table class="cell-spacing">
            <tr>
                <td>IČ:</td>
                <td><?= htmlspecialchars($seller['identification_number'], true) ?></td>
            </tr>
            <?php if ($seller['tax_number']) : ?>
                <tr>
                    <td>DIČ:</td>
                    <td><?= htmlspecialchars($seller['tax_number'], true) ?></td>
                </tr>
            <?php endif ?>
        </table>
        <p>
            <?= htmlspecialchars($seller['registry_entry'],true) ?>
        </p>
    </div>

</div>