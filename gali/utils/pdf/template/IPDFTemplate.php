<?php
	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 20.6.2016
	 * Time: 10:34
	 */

	namespace Gali\Utils\Pdf\Template;


	interface IPDFTemplate
	{
		/**
		 * zavolá šablonu a vloží data. Vrátí výsledené html
		 * @param $data
		 * @return mixed
		 */
		public function generateHtml($data);

		/**
		 * Zde definujte jméno šablony
		 * @return mixed
		 */
		public function getSource();
	}