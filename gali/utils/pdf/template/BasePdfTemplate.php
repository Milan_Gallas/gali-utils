<?php
	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 20.6.2016
	 * Time: 14:13
	 */

	namespace Gali\Utils\Pdf\Template;


	/**
	 * Class BasePdfTemplate
	 *
	 * @package App\AdminModule\Model\PDF\Templates
	 */
	abstract class BasePdfTemplate implements IPDFTemplate
	{

		/**
		 * Vrátím html ze šablony, doplněné daty
		 * @param $data
		 * @return string
		 */
		public function generateHtml($data)
		{
			$html = $this->getDataFromTemplate($data, $this->getSource());
			//teď můžu html dále zpracovávat.
			return $html;
		}

		/**
		 * Zavolám šablonu a uložím data, které se poté vypíší
		 *
		 * @param $data
		 * @return string
		 */
		protected function getDataFromTemplate($data, $source)
		{
			//rozdělím hlavní pole
			extract($data);
			// Zpracování šablony
			ob_start(); // Přesměrujeme výstup do bufferu
			require($source); // Zpracujeme šablonu
			$html = ob_get_contents();
			ob_end_clean(); // Přesměrujeme výstup zpět a vyčistíme buffer
			return $html;
		}

		public abstract function getSource();
	}