<?php
	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 20.6.2016
	 * Time: 10:52
	 */

	namespace Gali\Utils\Pdf\Facade;
	use Gali\Utils\Pdf\PDFCreator;
	use Gali\Utils\Pdf\Template\IPDFTemplate;

	class BasePDFFacade
	{

		/** @var PDFCreator třída pro vytváření pdf dokumentů */
		protected $pdfCreator;

		/** @var  IPDFTemplate zavolá šablonu a přeadá data */
		protected $template;

		/** @var string - typ výstupu */
		protected $output;

		/**
		 * TestFacade constructor.
		 *
		 * @param PDFCreator $PDFCreator
		 */
		public function __construct(PDFCreator $PDFCreator)
		{
			$this->pdfCreator = $PDFCreator;
		}

		/**
		 * @param IPDFTemplate $template
		 * @return $this
		 */
		public function setTemplateClass(IPDFTemplate $template)
		{
			$this->template = $template;
			return $this;
		}

		public function setOutput($output)
		{
			$this->output = $output;
			return $this;
		}

		/**
		 * @param $data           - pole hodnot. velké pole se pak rozloží na proměnné, podle klíčů
		 * @param string $outputName
		 * @param null $cssSource - pokud chcete připojit css Styly, tak název
		 * @throws \Exception
		 */
		public function build($data, $outputName,$cssSource = null){
			if(!$this->output){
				throw new \Exception("pdf output is not defined");
			}

			$this->pdfCreator->printPDF(
				$this->template->generateHtml($data),
				$outputName,
				$cssSource,
				$this->output
			);
		}
	}